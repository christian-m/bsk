package bsk;

import static org.junit.Assert.*;

import org.hamcrest.core.IsNull;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test
	public void testFrameFirstThrow() throws Exception{
		//Arrange
		Frame frame = new Frame (2,4);

		//Assert
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testFrameSecondThrow() throws BowlingException{
		//Arrange
		Frame frame = new Frame (2,4);

		//Assert
		assertEquals(4, frame.getSecondThrow());
	}

	@Test
	public void testFrameSpare() throws BowlingException{
		//Arrange
		Frame frame = new Frame (8,2);
		//Assert
		
		assertEquals(true, frame.isSpare());
	}
	
	@Test
	public void testFrameSetBonus() throws BowlingException{
		//Arrange
		Frame frame = new Frame (8,2);
		//Act
		frame.setBonus(2);
		//Assert
		
		assertEquals(12, frame.getScore());
	}
	
	@Test
	public void testFrameGetBonus() throws BowlingException{
		//Arrange
		Frame frame = new Frame (8,2);
		//Act
		frame.setBonus(2);
		//Assert
		
		assertEquals(2, frame.getBonus());
	}
	
	@Test
	public void testFrameIsStrike() throws BowlingException{
		//Arrange
		Frame frame = new Frame (10,0);
		//Assert
		assertEquals(true, frame.isStrike());
	}
	
	@Test
	public void testFrameGetScoreWithStrike() throws BowlingException{
		//Arrange
		Frame frame = new Frame (10,0);
		Frame frame2 = new Frame (3,6);
		
		//Act
		frame.setBonus(frame2.getScore());
		//Assert
		assertEquals(19, frame.getScore());
	}
}//Fine Classe
