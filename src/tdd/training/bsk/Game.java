package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private ArrayList<Frame> frame;
	private int firstBonusThrow=0;
	private int secondBonusThrow=0;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() throws BowlingException{
		frame = new ArrayList<>();
		}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(this.frame.size() < 10)
			{
				this.frame.add(frame);
			}
		else throw new BowlingException("10 frames have already been inserted");
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		if(index >= 0 && index <=9) return this.frame.get(index);
		else throw new BowlingException("Choosen index is out of range. Index range[0-9]");
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow >=0 && firstBonusThrow <= 10) this.firstBonusThrow = firstBonusThrow;
		else throw new BowlingException("Bonus point/s are higher or lower than the starndard (0-10)");
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow >=0 && secondBonusThrow <= 10) this.secondBonusThrow = secondBonusThrow;
		else throw new BowlingException("Bonus point/s are higher or lower than the starndard (0-10)");
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		int sum=0;
		for (int i=0 ; i<this.frame.size() ; i++)
		{
			if(this.frame.get(i).isStrike())
			{
				if(i == 8)
				{
					this.frame.get(i).setBonus(this.frame.get(i+1).getFirstThrow()+getFirstBonusThrow());
					sum += this.frame.get(i).getScore();
				}
				else
				{
					sum += calculateScoreStrike(i);
				}
				
			}
			else if(this.frame.get(i).isSpare())
			{
				sum += calculateScoreSpare(i);
			}
			else
			{
				sum += this.frame.get(i).getScore();
			}
		}
		return sum;	
	}
	
public int calculateScoreStrike(int index)
	{
		int sum=0;
		if(index<8 && this.frame.get(index+1).isStrike())
		{
			this.frame.get(index).setBonus(this.frame.get(index+1).getScore()+this.frame.get(index+2).getFirstThrow());
			sum += this.frame.get(index).getScore();
		}
		else if(index==9)
		{
			sum += this.frame.get(index).getScore()+this.firstBonusThrow+this.secondBonusThrow;
		}
		else
		{
			this.frame.get(index).setBonus(this.frame.get(index+1).getScore());
			sum += this.frame.get(index).getScore();
		}
		return sum;
	}

public int calculateScoreSpare(int index)
	{
		int sum=0;
		
		if(index < 9)
		{
			this.frame.get(index).setBonus(this.frame.get(index+1).getFirstThrow());
			sum += this.frame.get(index).getScore();
		}
		else if(index == 9)
		{
				sum += this.frame.get(index).getScore()+this.firstBonusThrow;
		}
		
		return sum;
	}

}

 